package com.paic.arch.jmsbrokerRefactor;

import static com.paic.arch.jmsbroker.SocketFinder.findNextAvailablePortBetween;
import static org.slf4j.LoggerFactory.getLogger;

import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;

/**
 * 为降低耦合度，尽量符合单一职责的原则，对原来的API进行了拆分
 * 1、通过JmsMessageProducer，实现消息的发送
 * 2、通过JmsMessageConsumer，实现消息的接收
 * @author DELL
 *
 */
public class JmsMessageBroker{
	
	private static final Logger LOG = getLogger(JmsMessageBroker.class);
	
    private BrokerService brokerService;
    
    private String brokerUrl;
    
    private String destinationName;
    
    private String inputMessage;
   
    private static final int ONE_SECOND = 1000;
    
    private static final int DEFAULT_RECEIVE_TIMEOUT = 10 * ONE_SECOND;
	
	public static final String DEFAULT_BROKER_URL_PREFIX = "tcp://localhost:";
	
	public JmsMessageBroker(String aBrokerUrl){
		brokerUrl=aBrokerUrl;
	}
	
	public static JmsMessageBroker createARunningEmbeddedBrokerOnAvailablePort() throws Exception {
        return createARunningEmbeddedBrokerAt(DEFAULT_BROKER_URL_PREFIX + findNextAvailablePortBetween(41616, 50000));
    }
	
	private void createEmbeddedBroker() throws Exception {
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(brokerUrl);
    }
	
	private void startEmbeddedBroker() throws Exception {
        brokerService.start();
    }
	
	public static JmsMessageBroker createARunningEmbeddedBrokerAt(String aBrokerUrl) throws Exception {
        LOG.debug("Creating a new broker at {}", aBrokerUrl);
        JmsMessageBroker broker = bindToBrokerAtUrl(aBrokerUrl);
        broker.createEmbeddedBroker();
        broker.startEmbeddedBroker();
        return broker;
    }
	
	public static JmsMessageBroker bindToBrokerAtUrl(String aBrokerUrl){
		return new JmsMessageBroker(aBrokerUrl);
		
	}
	
	public final JmsMessageBroker and(){
		return this;
	}
	
	public JmsMessageBroker sendTheMessage(String inputMessage){
		this.inputMessage=inputMessage;
		return this;
	}
	
	public JmsMessageBroker to(String inputQueue){
		this.destinationName=inputQueue;
		return this;
	}
	
	public JmsMessageBroker andThen(){
		return this;
	}
	
	public String waitForAMessageOn(String accountingQueue){
		
		JmsMessageQueue jmsMessageQueue=new JmsMessageQueue(this.brokerUrl,this.destinationName);
		
		JmsMessageProducer producer=new JmsMessageProducer(jmsMessageQueue);
		producer.sendMessage(this.inputMessage);
		
		JmsMessageConsumer consumer=new JmsMessageConsumer(jmsMessageQueue);
		return consumer.receiveMsg(DEFAULT_RECEIVE_TIMEOUT);
	}

	
}
