package com.paic.arch.jmsbrokerRefactor;

import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;

public class JmsMessageQueue {
	
	private static final Logger LOG = getLogger(JmsMessageBroker.class);
	
	public String brokerUrl;
	private Connection MQconnection;
	private Session session;
	public String destinationName;
	private Queue queue;
	
	public JmsMessageQueue(String brokerUrl,String destinationName){
		this.brokerUrl=brokerUrl;
		this.destinationName=destinationName;
		this.initConnection();
		this.initSession();
		this.initQueue();
	}
	
	/**
	 * 创建连接
	 */
	private void initConnection() {
        try {
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
            MQconnection = connectionFactory.createConnection();
            MQconnection.start();
            
        } catch (JMSException jmse) {
            LOG.error("failed to create connection to {}", brokerUrl);
            throw new IllegalStateException(jmse);
        } finally {
            if (MQconnection != null) {
                try {
                	MQconnection.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close connection to broker at []", brokerUrl);
                    throw new IllegalStateException(jmse);
                }
            }
        }
    }
	
	/**
	 * 建立会话
	 */
	private void initSession(){
        try {
            session = MQconnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException jmse) {
            LOG.error("Failed to create session on connection {}", MQconnection);
            throw new IllegalStateException(jmse);
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close session {}", session);
                    throw new IllegalStateException(jmse);
                }
            }
        }
	}
	
	private void initQueue(){
		try {
			queue=session.createQueue(destinationName);
		} catch (JMSException e) {
			LOG.error("Failed to create queue on session {}", session);
            throw new IllegalStateException(e);
		}
	}
	
	public Session getSession(){
		return session;
	}
	
	public Queue getQueue(){
		return queue;
	}
}
