package com.paic.arch.jmsbrokerRefactor;

import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.TextMessage;

import org.slf4j.Logger;

public class JmsMessageConsumer{
	
	private static final Logger LOG = getLogger(JmsMessageConsumer.class);
	
	private static MessageConsumer consumer;
	
	private JmsMessageQueue jmsMessageQueue;
	
	public JmsMessageConsumer(JmsMessageQueue jmsMessageQueue){
		this.jmsMessageQueue=jmsMessageQueue;
		this.initConsumer();
	}
	
	private void initConsumer(){
		try {
			consumer=jmsMessageQueue.getSession().createConsumer(jmsMessageQueue.getQueue());
		} catch (JMSException e) {
			LOG.error("failed to create consumer on queue {}", jmsMessageQueue.getQueue());
            throw new IllegalStateException(e);
		}finally{
			if (consumer != null) {
                try {
                	consumer.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close consumer {}", consumer);
                    throw new IllegalStateException(jmse);
                }
            }
		}
	}
	
	public String receiveMsg(int aTimeout){
		Message message =null;
		String msg="";
         try {
        	 message = consumer.receive(aTimeout);
             if (message == null) {
                 throw new NoMessageReceivedException(String.format("No messages received from the broker within the %d timeout", aTimeout));
             }
			consumer.close();
			msg=((TextMessage) message).getText();
		} catch (JMSException e) {
			LOG.error("consumer failed to operation {}", consumer);
            throw new IllegalStateException(e);
		}finally{
			if (consumer != null) {
                try {
                	consumer.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close consumer {}", consumer);
                    throw new IllegalStateException(jmse);
                }
            }
		}
         return msg;
	}
	
	
	public class NoMessageReceivedException extends RuntimeException {
        
		private static final long serialVersionUID = 4772612528271857588L;

		public NoMessageReceivedException(String reason) {
            super(reason);
        }
    }
	
}
