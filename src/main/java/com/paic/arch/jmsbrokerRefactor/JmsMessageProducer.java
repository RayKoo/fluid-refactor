package com.paic.arch.jmsbrokerRefactor;

import static org.slf4j.LoggerFactory.getLogger;

import javax.jms.JMSException;
import javax.jms.MessageProducer;

import org.slf4j.Logger;

public class JmsMessageProducer{
	
	private static final Logger LOG = getLogger(JmsMessageProducer.class);
	
	private static MessageProducer producer;
	
	private JmsMessageQueue jmsMessageQueue; 
	
	public JmsMessageProducer(JmsMessageQueue jmsMessageQueue){
		this.jmsMessageQueue=jmsMessageQueue;
		this.initProducer();
	}
	
	private void initProducer(){
		 try {
			producer = jmsMessageQueue.getSession().createProducer(jmsMessageQueue.getQueue());
		} catch (JMSException e) {
			LOG.error("failed to create producer on queue {}", jmsMessageQueue.getQueue());
            throw new IllegalStateException(e);
		}finally{
			if (producer != null) {
                try {
                	producer.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close producer {}", producer);
                    throw new IllegalStateException(jmse);
                }
            }
		}
	}
	
	public void sendMessage(String inputMessage){
		try {
			producer.send(jmsMessageQueue.getSession().createTextMessage(inputMessage));
			producer.close();
		} catch (JMSException e) {
			LOG.error("producer failed to operation {}", producer);
            throw new IllegalStateException(e);
		}finally{
			if (producer != null) {
                try {
                	producer.close();
                } catch (JMSException jmse) {
                    LOG.warn("Failed to close producer {}", producer);
                    throw new IllegalStateException(jmse);
                }
            }
		}
		//return jmsMessageQueue;
	}
	

}
